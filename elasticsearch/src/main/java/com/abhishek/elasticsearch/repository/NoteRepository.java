package com.abhishek.elasticsearch.repository;

import com.abhishek.elasticsearch.entities.Note;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface NoteRepository extends ElasticsearchRepository<Note, Long> {
}
