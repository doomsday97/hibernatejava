package com.abhishek.elasticsearch.controller;


import com.abhishek.elasticsearch.entities.Note;
import com.abhishek.elasticsearch.service.hibernate_services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
public class NoteController {

    @Autowired
    hibernate_services hibernate_services;



    @GetMapping("/notes")
    public Iterable<Note> getAllNotes() {
        return hibernate_services.getallnotes();
    }

    @PutMapping(value = "/notes")//, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Note createNote (@Valid @RequestBody Note note) throws Exception {
        System.out.println("Addition performed successfully");
        return hibernate_services.createNote(note);
    }

    @GetMapping("/notes/{id}")
    public Note getNoteById(@PathVariable(value = "id") long noteid){
        return hibernate_services.getNoteById(noteid);
    }
}






//@GetMapping("/notes/{id}")
//    public Note getNoteById(@PathVariable(value = "id") long noteid){
//        return hibernate_services.getNoteById(noteid);
//    }
//
//    @DeleteMapping("/notes/{id}")
//    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long noteId) {
//        System.out.println("deletion performed successfully");
//        return hibernate_services.deleteNote(noteId);
//    }
//
//
//    @PutMapping(value = "/notes/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
//    public Note updateNote(@PathVariable(value = "id") Long noteId,
//                           @Valid @RequestBody Note noteDetails) {
//        System.out.println("updation performed successfully");
//        return hibernate_services.updateNote(noteId, noteDetails);
//
//    }




