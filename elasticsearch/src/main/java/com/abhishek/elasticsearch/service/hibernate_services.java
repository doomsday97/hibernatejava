package com.abhishek.elasticsearch.service;


import com.abhishek.elasticsearch.controller.ResourceNotFound;
import com.abhishek.elasticsearch.entities.Note;

import com.abhishek.elasticsearch.repository.NoteRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.ArrayList;
import java.util.UUID;

@Service
public class hibernate_services {

    @Autowired
    RestHighLevelClient restHighLevelClient;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    NoteRepository noteRepository;

    public Note getNoteById(Long noteId) {
        return noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFound("Note", "id", noteId));
    }


    public Iterable<Note> getallnotes()
    {
        return noteRepository.findAll();
    }

    public Note createNote(Note note) throws Exception{

        return noteRepository.save(note);


    }

}



