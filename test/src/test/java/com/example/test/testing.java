package com.example.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;



@SpringBootTest
@RunWith(SpringRunner.class)
public class testing {

    private MockMvc mockMvc;

    @Autowired
   WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void add() throws Exception{
        String vehicle = "{ \n" + "\"id\": \"14\",\n" + "\"year\": 2019,\n" + "\"make\":\"FCA\",\n" + "\"model\":\"RAM\",\n" + "\"vin\": \"1A4AABBC5KD501999\",\n" + "\"transmissionType\":\"MANUAL\"\n" + "}";

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/vehicle-api/v1/vehicles/vehicle").accept(MediaType.APPLICATION_JSON).content(vehicle).contentType(MediaType.APPLICATION_JSON);
       // MvcResult def = mockMvc.perform(post("/vehicle-api/v1/vehicles/vehicle").content(vehicle)).andReturn();

        int x = mockMvc.perform(requestBuilder).andReturn().getResponse().getStatus();

        Assert.assertEquals(200,x);
    }

}
