package com.example.test.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import  java.util.*;


@Entity
@Table(name = "vehicle")
public class Vehicle implements Serializable {
    String vin;
    int year;
    String make;
    String model;
    String transmissionType;


    @Id String id = UUID.randomUUID().toString();

    public Vehicle(String vin, int year, String make, String model, String transmissionType) {
        this.vin = vin;
        this.year = year;
        this.make = make;
        this.model = model;
        this.transmissionType = transmissionType;
    }

    public Vehicle() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
  }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(String transmissionType) {
        this.transmissionType = transmissionType;
    }

    @Override
    public String toString() {
        return "UserEvent{" +
                "vin:'" + vin + '\'' +
                ", year:" + year +
                ", make:'" + make + '\'' +
                ", model:'" + model + '\'' +
                ", transmissionType:'" + transmissionType + '\'' +
                '}';
    }
}
