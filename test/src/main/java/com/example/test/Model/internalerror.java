package com.example.test.Model;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class internalerror extends  RuntimeException {
    private HttpStatus statusCode;
    private String errorMessage;

    public internalerror(HttpStatus statuscode, String errormessage){
        this.statusCode=statuscode;
        this.errorMessage=errormessage;
    }


    public HttpStatus getStatusCode(){
        return statusCode;
    }

    public String getErrorMessage(){
        return errorMessage;
    }
}
