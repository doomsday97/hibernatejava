package com.example.test.Model;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ErrorHandler extends RuntimeException{
    private HttpStatus statusCode;
    private String errorMessage;

  public ErrorHandler(HttpStatus statuscode, String errormessage){
     this.statusCode=statuscode;
     this.errorMessage=errormessage;
  }


    public HttpStatus getStatusCode(){
        return statusCode;
    }

    public String getErrorMessage(){
        return errorMessage;
    }

}
