package com.example.test.servivce;

import com.example.test.Model.Vehicle;
import com.example.test.Repository.vehiclerepo;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;

import java.util.concurrent.CompletableFuture;


@Service
public class FirstQuestionService {


    @Autowired
    vehiclerepo vehiclerepo;

    @Async("asyncExecutor")
    public Object getId(Vehicle vehicle){
        Logger log = LoggerFactory.getLogger(FirstQuestionService.class);

        log.info("Request Payload {}", vehicle.toString());
        log.info("Vehicle ID: {}", vehicle.getVin());
        vehiclerepo.save(vehicle);
        log.info("Vehicle Post Data: {}", vehicle.toString());
        return CompletableFuture.completedFuture(vehicle.getVin());
    }
}
