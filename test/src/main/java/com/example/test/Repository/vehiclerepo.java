package com.example.test.Repository;

import com.example.test.Model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface vehiclerepo extends JpaRepository<Vehicle, Long> {
}
