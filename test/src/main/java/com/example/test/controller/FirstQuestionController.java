package com.example.test.controller;


import com.example.test.Model.ErrorHandler;
import com.example.test.Model.Vehicle;
import com.example.test.Model.internalerror;
import com.example.test.servivce.FirstQuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.concurrent.ExecutionException;

@RestController
public class FirstQuestionController {

    @Autowired
    FirstQuestionService firstQuestionService;

    Logger log = LoggerFactory.getLogger(FirstQuestionController.class);

    @PostMapping("/vehicle-api/v1/vehicles/vehicle")
    public ResponseEntity<?> getId(@Valid @RequestBody Vehicle vehicle) throws ExecutionException, InterruptedException {

        System.out.println("Data" + vehicle.toString());
        log.info("Request Payload {}", vehicle.toString());
        if(vehicle.getTransmissionType() == null || vehicle.getModel() == null || vehicle.getMake() == null || vehicle.getYear() == 0) {
            throw  new internalerror(HttpStatus.INTERNAL_SERVER_ERROR, "Payload canot be null");
        }
        else if (!(vehicle.getTransmissionType().equals("MANUAL") || vehicle.getTransmissionType().equals("AUTO"))) {
            throw new ErrorHandler(HttpStatus.BAD_REQUEST, "Transmission type should be MANUAL or AUTO");
        }else{
            return new ResponseEntity<>(firstQuestionService.getId(vehicle), HttpStatus.OK);
        }
    }

}
