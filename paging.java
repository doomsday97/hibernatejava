
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.*;

public class Paging{

    public static void main(String[] args){
        List<ArrayList<String>> items = new ArrayList<ArrayList<String>>();
        items.add(new ArrayList<String>(Arrays.asList("Mike", "4", "13" )));
        items.add(new ArrayList<String>(Arrays.asList("Emily", "2", "21")));
        items.add(new ArrayList<String>(Arrays.asList("James", "3", "12")));

        Scanner scanner = new Scanner(System.in);

        int sortparameter = scanner.nextInt();
        int sortorder = scanner.nextInt();
        int itemperpage = scanner.nextInt();
        int pagenumber = scanner.nextInt();

        Collections.sort(items, new Comparator<ArrayList<String>>() {
            @Override
            public int compare(ArrayList<String> o1, ArrayList<String> o2) {
                if(sortorder==0)
                 return o1.get(sortparameter).compareTo(o2.get(sortparameter));
                else
                  return o2.get(sortparameter).compareTo(o1.get(sortparameter));
               }  
    
            });
            System.out.println(items);
            
            int page = pagenumber*itemperpage;
            for(int i =0; i<itemperpage;i++){
                System.out.println(items.get(page));
            }
    }
}