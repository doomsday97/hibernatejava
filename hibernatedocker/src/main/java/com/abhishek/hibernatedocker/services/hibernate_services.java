package com.abhishek.hibernatedocker.services;

import com.abhishek.hibernatedocker.controller.ResourceNotFound;
import com.abhishek.hibernatedocker.entities.Note;
import com.abhishek.hibernatedocker.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class hibernate_services {

    @Autowired
    NoteRepository noteRepository;

    public Note createNote(Note note) {

        return noteRepository.save(note);
    }

    public Note getNoteById(Long noteId) {
        return noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFound("Note", "id", noteId));
    }


    public Note updateNote(long noteId, Note noteDetails) {

        Note note = noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFound("Note", "id", noteId));

        note.setTitle(noteDetails.getTitle());
        note.setContent(noteDetails.getContent());

        Note updatedNote = noteRepository.save(note);
        return updatedNote;
    }



    public ResponseEntity<?> deleteNote(long noteId) {
        Note note = noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFound("Note", "id", noteId));

        noteRepository.delete(note);

        return ResponseEntity.ok().build();
    }


    public List<Note> getallnotes() {

        return noteRepository.findAll();
    }

    public List<Note> getAllByPrefrence(String title)
    {
        List<Note> allData = noteRepository.findAll();
        return filterList(allData,title);

    }

    public List<Note> filterList(List<Note> notesList,String title)
    {
        List notes = new ArrayList();
        for(Note note: notesList){
            if(note.getTitle().equals(title)){

                notes.add(note);
            }
        }
        return notes;
    }
}
